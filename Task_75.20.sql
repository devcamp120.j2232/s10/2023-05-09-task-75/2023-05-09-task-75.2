-- Hiển thị toàn bộ thông tin của tất cả sinh viên
SELECT * FROM `students`
-- Hiển thị mã sinh viên và username của tất cả sinh viên
SELECT `student_code`,`user_name` FROM `students`
-- Hiển thị toàn bộ thông tin môn học, mà có credit (số tin chỉ) > 3
SELECT * FROM `subjects` WHERE `credit`>3
-- Hiển thị toàn bộ thông tin điểm có ngày thi (exam_date) trước ngày 01/05/2021
SELECT * FROM `grades` WHERE `exam_date`< "2021-05-01"
-- Hiển thị thông tin bảng điểm có các thông tin: student_code, subject_id, grade và exam_date 
SELECT  students.student_code, grades.subject_id, grades.grade, grades.exam_date FROM students INNER JOIN grades ON students.id= grades.student_id
-- Hiển thị thông tin bảng điểm có các thông tin: student_code, fullname, subject_id, grade và exam_date của môn có subject_id = 1
SELECT  students.student_code, CONCAT(students.first_name," ", students.last_name) AS fullname, grades.grade, grades.exam_date FROM students INNER JOIN grades ON students.id= grades.student_id WHERE grades.subject_id=1
-- Hiển thị toàn bộ thông tin bảng điểm: Tên môn, mã sinh viên, họ và tên, điểm, ngày thi
SELECT  subjects.subject_name, students.student_code, CONCAT(students.first_name," ", students.last_name) AS fullname, grades.grade, grades.exam_date FROM students INNER JOIN grades ON students.id= grades.student_id INNER JOIN subjects ON subjects.id = grades.subject_id
-- Tổng hợp xem mỗi môn có bao nhiêu học viên đã thi: lấy ra subject_id và số lượng tương ứng
SELECT g.subject_id, COUNT(g.student_id) FROM grades AS g GROUP BY g.subject_id
-- Tổng hợp ra các môn có số lượng học viên đã thi > 5. Lấy ra subject_id và số lượng tương ứng
SELECT g.subject_id, COUNT(g.student_id) FROM grades AS g GROUP BY g.subject_id HAVING COUNT(g.student_id)> 5
-- Tổng hợp ra các môn có số lượng học viên đã thi > 5. Lấy ra tên môn và số lượng tương ứng
SELECT g.subject_id, s.subject_name, COUNT(g.student_id) FROM grades AS g INNER JOIN subjects AS s ON s.id = g.subject_id GROUP BY g.subject_id HAVING COUNT(g.student_id)> 5 
-- Lấy ra thông tin 3 điểm thi cao nhất: môn học, tên sinh viên, điểm thi, ngày thi
SELECT sj.subject_name, CONCAT(s.first_name," ", s.last_name), g.grade, g.exam_date FROM subjects AS sj INNER JOIN grades AS g ON g.subject_id = sj.id INNER JOIN students AS s ON s.id = g.student_id ORDER BY g.grade DESC LIMIT 3

